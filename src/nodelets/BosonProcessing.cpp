/*
 * Copyright © 2019 AutonomouStuff, LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the “Software”), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <pluginlib/class_list_macros.h>
#include "BosonProcessing.h"

PLUGINLIB_EXPORT_CLASS(flir_boson_usb::BosonProcessing, nodelet::Nodelet)

using namespace cv;
using namespace flir_boson_usb;


BosonProcessing::BosonProcessing() :
  cv_img()
{

}

BosonProcessing::~BosonProcessing(){
  free(left_camera);
  free(right_camera);
}

void BosonProcessing::publishImagesSynchronous(const ros::TimerEvent& evt){
  //A time sample is taken prior to receiving the images.
  //This is to make sure that both left and right camera's have the same time stamp for synchronisation purposes
  ros::Time tmp_time = ros::Time::now();

  //We are parsing time as an argument to ensure that header timers are synced
  left_camera->captureAndPublish16b(tmp_time);
  right_camera->captureAndPublish16b(tmp_time);
}

void BosonProcessing::openCameras()
{
  //Parameter loading
  pnh.param<std::string>("frame_id", frame_id, "boson_camera");
  pnh.param<std::string>("dev_left", dev_path_left, "/dev/video0");
  pnh.param<std::string>("dev_right", dev_path_right, "/dev/video2");
  pnh.param<float>("frame_rate", frame_rate, 60.0);
  pnh.param<std::string>("video_mode_left", video_mode_str, "RAW16");
  pnh.param<bool>("zoom_enabel", zoom_enable, false);
  pnh.param<std::string>("sensor_type_left", sensor_type_str, "Boson_640");
  pnh.param<std::string>("camera_info_url", camera_info_url, "");

  ROS_INFO("flir_boson_usb - Got frame_id: %s.", frame_id.c_str());
  ROS_INFO("flir_boson_usb - Got dev: %s.", dev_path_left.c_str());
  ROS_INFO("flir_boson_usb - Got frame rate: %f.", frame_rate);
  ROS_INFO("flir_boson_usb - Got video mode: %s.", video_mode_str.c_str());
  ROS_INFO("flir_boson_usb - Got zoom enable: %s.", (zoom_enable ? "true" : "false"));
  ROS_INFO("flir_boson_usb - Got sensor type: %s.", sensor_type_str.c_str());
  ROS_INFO("flir_boson_usb - Got camera_info_url: %s.", camera_info_url.c_str());

  //-----------------------------------------Left camera-----------------------------------------------//
  left_camera = new BosonCamera(frame_id.c_str(),
                                dev_path_left.c_str(),
                                frame_rate,
                                zoom_enable,
                                camera_info_url.c_str(),
                                video_mode_str.c_str(),
                                sensor_type_str.c_str(),
                                true //left camera = true, right camera = false
  );

  //-----------------------------------------Right camera-----------------------------------------------//
  right_camera = new BosonCamera(frame_id.c_str(),
                              dev_path_right.c_str(),
                              frame_rate,
                              zoom_enable,
                              camera_info_url.c_str(),
                              video_mode_str.c_str(),
                              sensor_type_str.c_str(),
                              false //left camera = true, right camera = false
  );

  //This calls the function periodically to receive images from the cameras
  capture_timer = nh.createTimer(ros::Duration(1.0 / frame_rate),
      boost::bind(&BosonProcessing::publishImagesSynchronous, this, _1));

}

void BosonProcessing::onInit()
{
  nh = getNodeHandle();
  pnh = getPrivateNodeHandle();

  openCameras();
}



