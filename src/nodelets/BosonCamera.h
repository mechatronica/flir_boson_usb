#ifndef FLIR_BOSON_USB_BOSONCAMERA_H
#define FLIR_BOSON_USB_BOSONCAMERA_H

#endif  // FLIR_BOSON_USB_BOSONCAMERA_H

// C++ Includes
#include <string>

// Linux system includes
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <asm/types.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/videodev2.h>

// OpenCV Includes
#include <opencv2/opencv.hpp>

// ROS Includes
#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <camera_info_manager/camera_info_manager.h>

#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>

using namespace std;

namespace flir_boson_usb
{
    enum Encoding
    {
        YUV = 0,
        RAW16 = 1
    };

    enum SensorTypes
    {
        Boson320,
        Boson640
    };

    class BosonCamera
    {
        public:
            BosonCamera(
                string frame_id,
                string dev_path,
                float frame_rate,
                bool zoom_enable,
                string camera_info_url,
                string video_mode_str,
                string sensor_type_str,
                bool is_left_camera
                );

           ~BosonCamera(); 
            void captureAndPublish16b(ros::Time time_now);

        private:
            //functions
            bool openCamera();
            bool closeCamera();
            void agcBasicLinear(const cv::Mat& input_16,
                    cv::Mat* output_8,
                    const int& height,
                    const int& width);

            //camera parameters (could be in constructor)
            string frame_id;
            string dev_path;
            float frame_rate;
            bool zoom_enable;
            string camera_info_url;
            bool is_left_camera;

            sensor_msgs::CameraInfoPtr cameraInfo;

            //These parameters need to be parsed
            Encoding video_mode;
            SensorTypes sensor_type;

            //OpenCV
            cv::Mat thermal16;
            cv::Mat thermal16_linear;
            cv::Mat thermal16_linear_zoom;
            cv::Mat thermal_rgb_zoom;
            cv::Mat thermal_luma;
            cv::Mat thermal_rgb;
            
            sensor_msgs::CameraInfoPtr ci_cpy;

            //8BIT
            cv_bridge::CvImage cv_img;
            sensor_msgs::ImagePtr pub_image;
            image_transport::CameraPublisher image_pub; 
            std::shared_ptr<image_transport::ImageTransport> it; 

            //16BIT
            cv_bridge::CvImage cv_img_16b;
            sensor_msgs::ImagePtr pub_image_16b;
            image_transport::CameraPublisher image_pub_16b; 
            std::shared_ptr<image_transport::ImageTransport> it_16b;

            //Camera manager
            shared_ptr<camera_info_manager::CameraInfoManager> camera_info;
            int32_t width, height;
            int32_t fd;
            struct v4l2_capability cap;
            int32_t frame = 0;                // First frame number enumeration
            int8_t thermal_sensor_name[20];  // To store the sensor name
            struct v4l2_buffer bufferinfo;
            void* buffer_start;

            //ROS stuff
            ros::NodeHandle nh;
            ros::NodeHandle *parent_nh; //TODO discuss if this should be in camera or parent class or pointer maybe?

            int32_t i; //does this really need to be a parameter?
    };
}