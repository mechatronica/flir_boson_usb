/*
 * Copyright © 2019 AutonomouStuff, LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the “Software”), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef IMAGE_LINEARIZER_H
#define IMAGE_LINEARIZER_H

// C++ Includes
#include <string>

#include "BosonCamera.h"

// Linux system includes
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <asm/types.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/videodev2.h>

// OpenCV Includes
#include <opencv2/opencv.hpp>

// ROS Includes
#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <camera_info_manager/camera_info_manager.h>

#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>


// enum Encoding
// {
//     YUV = 0,
//     RAW16 = 1
// };
void agcBasicLinear(const cv::Mat& input_16,
        cv::Mat* output_8,
        const int& height,
        const int& width);

cv_bridge::CvImage cv_img;
sensor_msgs::ImagePtr pub_image;
image_transport::CameraPublisher image_pub_left; 
image_transport::CameraPublisher image_pub_right; 
std::shared_ptr<image_transport::ImageTransport> it; 


// Fillable paramters for cameras
std::string frame_id;
std::string dev_path_left;
std::string dev_path_right;
std::string camera_info_url;
std::string video_mode_str;
std::string sensor_type_str;
float frame_rate;
bool zoom_enable;

cv::Mat thermal16_linear;

sensor_msgs::CameraInfoPtr cameraInfo;
shared_ptr<camera_info_manager::CameraInfoManager> camera_info;
ros::Timer capture_timer;




#endif  // IMAGE_LINEARIZER_H
