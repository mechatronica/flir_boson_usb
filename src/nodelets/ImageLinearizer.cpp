#include "ImageLinearizer.h"
using namespace cv;

void agcBasicLinear(const Mat& input_16,
                                 Mat* output_8,
                                 const int& height,
                                 const int& width)
{
  int i, j;  // aux variables

  // auxiliary variables for AGC calcultion
  unsigned int max1 = 0;         // 16 bits
  unsigned int min1 = 0xFFFF;    // 16 bits
  unsigned int value1, value2, value3, value4;

  // RUN a super basic AGC
  for (i = 0; i < height; i++)
  {
    for (j = 0; j < width; j++)
    {
      value1 = input_16.at<uchar>(i, j * 2 + 1) & 0xFF;  // High Byte
      value2 = input_16.at<uchar>(i, j * 2) & 0xFF;      // Low Byte
      value3 = (value1 << 8) + value2;

      if (value3 <= min1)
        min1 = value3;

      if (value3 >= max1)
        max1 = value3;
    }
  }

  for (int i = 0; i < height; i++)
  {
    for (int j = 0; j < width; j++)
    {
      value1 = input_16.at<uchar>(i, j * 2 + 1) & 0xFF;     // High Byte
      value2 = input_16.at<uchar>(i, j * 2) & 0xFF;         // Low Byte
      value3 = (value1 << 8) + value2;
      value4 = ((255 * (value3 - min1))) / (max1 - min1);

      output_8->at<uchar>(i, j) = static_cast<uint8_t>(value4 & 0xFF);
    }
  }
}

void linearize_image(const sensor_msgs::Image::ConstPtr& msg){
  cv_bridge::CvImagePtr cv_ptr;
  cv_ptr = cv_bridge::toCvCopy(msg, "mono16");

  Mat thermal16 = cv_ptr->image;

  int width = 640;
  int height = 512;

// OpenCV output buffer : Data used to display the video
  thermal16_linear = Mat(height, width, CV_8U, 1);


  Size size(640, 512);
//       // ---------------Start linearizing--------------//
//   if (video_mode == RAW16){
  agcBasicLinear(thermal16, &thermal16_linear, height, width);

  // Display thermal after 16-bits AGC... will display an image
  // if (!zoom_enable){
  // Threshold using Otsu's method, then use the result as a mask on the original image
  Mat mask_mat, masked_img;
  threshold(thermal16_linear, mask_mat, 0, 255, CV_THRESH_BINARY|CV_THRESH_OTSU);
  thermal16_linear.copyTo(masked_img, mask_mat);

  // Normalize the pixel values to the range [0, 1] then raise to power (gamma). Then convert back for display.
  Mat d_out_img, norm_image, d_norm_image, gamma_corrected_image, d_gamma_corrected_image;
  double gamma = 0.8;
  masked_img.convertTo(d_out_img, CV_64FC1);
  normalize(d_out_img, d_norm_image, 0, 1, NORM_MINMAX, CV_64FC1);
  pow(d_out_img, gamma, d_gamma_corrected_image);
  d_gamma_corrected_image.convertTo(gamma_corrected_image, CV_8UC1);
  normalize(gamma_corrected_image, gamma_corrected_image, 0, 255, NORM_MINMAX, CV_8UC1);

  // Apply top hat filter
  int erosion_size = 5;
  Mat top_hat_img, kernel = getStructuringElement(MORPH_ELLIPSE,
      Size(2 * erosion_size + 1, 2 * erosion_size + 1));
  morphologyEx(gamma_corrected_image, top_hat_img, MORPH_TOPHAT, kernel);

  cv_img.image = thermal16_linear;
  cv_img.header.stamp = ros::Time::now();
  cv_img.header.frame_id = frame_id;
  cv_img.encoding = "mono8";
  pub_image = cv_img.toImageMsg();

  // cameraInfo->header.stamp = pub_image->header.stamp;

}

void left_camera_callback(const sensor_msgs::Image::ConstPtr& msg)
{
  linearize_image(msg);
  image_pub_left.publish(pub_image, cameraInfo);
}

void right_camera_callback(const sensor_msgs::Image::ConstPtr& msg)
{
  linearize_image(msg);
  image_pub_right.publish(pub_image, cameraInfo);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "listener");

    ros::NodeHandle nh;

    camera_info = std::shared_ptr<camera_info_manager::CameraInfoManager>(
    new camera_info_manager::CameraInfoManager(nh));

    ros::Subscriber left_cam_sub = nh.subscribe("/flir_boson/image_raw_left_16b", 1000, left_camera_callback);
    ros::Subscriber right_cam_sub = nh.subscribe("/flir_boson/image_raw_right_16b", 1000, right_camera_callback);

    it     = std::shared_ptr<image_transport::ImageTransport>(new image_transport::ImageTransport(nh));
    image_pub_left  = it->advertiseCamera("/flir_boson/image_raw_left_8b", 1);
    image_pub_right  = it->advertiseCamera("/flir_boson/image_raw_right_8b", 1);
    ros::spin();

    return 0;   
}