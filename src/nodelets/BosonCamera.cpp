#include <pluginlib/class_list_macros.h>
#include "BosonCamera.h"

using namespace cv;
using namespace flir_boson_usb;

BosonCamera::BosonCamera(string frame_id,
                        string dev_path,
                        float frame_rate,
                        bool zoom_enable,
                        string camera_info_url,
                        string video_mode_str,
                        string sensor_type_str,
                        bool is_left_camera){

  this->frame_id = frame_id;
  this->dev_path = dev_path;
  this->frame_rate = frame_rate;
  this->zoom_enable = zoom_enable;
  this->camera_info_url = camera_info_url;
  

  it     = std::shared_ptr<image_transport::ImageTransport>(new image_transport::ImageTransport(nh));
  it_16b = std::shared_ptr<image_transport::ImageTransport>(new image_transport::ImageTransport(nh));

  //VICTOR: change this to send string with node name? Makes it easier to upgrade to 6 camera's :)
  //TODO see Victor's suggestion
  string advertised_name = is_left_camera ? "image_raw_left" : "image_raw_right";
  string advertised_name_16b = is_left_camera ? "image_raw_left_16b" : "image_raw_right_16b";

  image_pub     = it->advertiseCamera(advertised_name, 1);
  image_pub_16b = it_16b->advertiseCamera(advertised_name_16b, 1);

  camera_info = std::shared_ptr<camera_info_manager::CameraInfoManager>(
  new camera_info_manager::CameraInfoManager(nh));

  bool exit = false;    
  if (video_mode_str == "RAW16"){
      video_mode = RAW16;
  }
  else if (video_mode_str == "YUV"){
      video_mode = YUV;
  }
  else{
      exit = true;
      ROS_ERROR("flir_boson_usb - Invalid video_mode value provided. Exiting.");
  }

  if (sensor_type_str == "Boson_320" || sensor_type_str == "boson_320"){
      sensor_type = Boson320;
      camera_info->setCameraName("Boson320");
  }
  else if (sensor_type_str == "Boson_640" || sensor_type_str == "boson_640"){
      sensor_type = Boson640;
      camera_info->setCameraName("Boson640");
  }
  else{
      exit = true;
      ROS_ERROR("flir_boson_usb - Invalid sensor_type value provided. Exiting.");
  }

  if (camera_info->validateURL(camera_info_url)){
      camera_info->loadCameraInfo(camera_info_url);
  }
  else{
      ROS_INFO("flir_boson_usb - camera_info_url could not be validated. Publishing with unconfigured camera.");
  }

  if (!exit){
      exit = openCamera() ? exit : true;
  }


  if(exit) {
      ros::shutdown();
      return;
  }
}

BosonCamera::~BosonCamera(){
  closeCamera(); 
}

bool BosonCamera::openCamera(){

  // Open the Video device
  if ((fd = open(dev_path.c_str(), O_RDWR)) < 0){
    ROS_ERROR("flir_boson_usb - ERROR : OPEN. Invalid Video Device.");
    return false;
  }

  // Check VideoCapture mode is available
  if (ioctl(fd, VIDIOC_QUERYCAP, &cap) < 0){
    ROS_ERROR("flir_boson_usb - ERROR : VIDIOC_QUERYCAP. Video Capture is not available.");
    return false;
  }

  if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)){
    ROS_ERROR("flir_boson_usb - The device does not handle single-planar video capture.");
    return false;
  }

  // Two different FORMAT modes, 8 bits vs RAW16
  struct v4l2_format format;
  if (video_mode == RAW16){
    // I am requiring thermal 16 bits mode
    format.fmt.pix.pixelformat = V4L2_PIX_FMT_Y16;

    // Select the frame SIZE (will depend on the type of sensor)
    switch (sensor_type){
      case Boson320:  // Boson320
        width = 320;
        height = 256;
        break;
      case Boson640:  // Boson640
        width = 640;
        height = 512;
        break;
      default:  // Boson320
        width = 320;
        height = 256;
        break;
    }
  }
  // 8- bits is always 640x512 (even for a Boson 320)
  else{
    format.fmt.pix.pixelformat = V4L2_PIX_FMT_YVU420;  // thermal, works   LUMA, full Cr, full Cb
    width = 640;
    height = 512;
  }

  // Common varibles
  format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  format.fmt.pix.width = width;
  format.fmt.pix.height = height;

  // request desired FORMAT
  if (ioctl(fd, VIDIOC_S_FMT, &format) < 0){
    ROS_ERROR("flir_boson_usb - VIDIOC_S_FMT error. The camera does not support the requested video format.");
    return false;
  }

  // we need to inform the device about buffers to use.
  // and we need to allocate them.
  // we'll use a single buffer, and map our memory using mmap.
  // All this information is sent using the VIDIOC_REQBUFS call and a
  // v4l2_requestbuffers structure:
  struct v4l2_requestbuffers bufrequest;
  bufrequest.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  bufrequest.memory = V4L2_MEMORY_MMAP;
  bufrequest.count = 1;   // we are asking for one buffer

  if (ioctl(fd, VIDIOC_REQBUFS, &bufrequest) < 0){
    ROS_ERROR("flir_boson_usb - VIDIOC_REQBUFS error. The camera failed to allocate a buffer.");
    return false;
  }

  // Now that the device knows how to provide its data,
  // we need to ask it about the amount of memory it needs,
  // and allocate it. This information is retrieved using the VIDIOC_QUERYBUF call,
  // and its v4l2_buffer structure.

  memset(&bufferinfo, 0, sizeof(bufferinfo));

  bufferinfo.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  bufferinfo.memory = V4L2_MEMORY_MMAP;
  bufferinfo.index = 0;

  if (ioctl(fd, VIDIOC_QUERYBUF, &bufferinfo) < 0){
    ROS_ERROR("flir_boson_usb - VIDIOC_QUERYBUF error. Failed to retreive buffer information.");
    return false;
  }

  // map fd+offset into a process location (kernel will decide due to our NULL). length and
  // properties are also passed
  buffer_start = mmap(NULL, bufferinfo.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, bufferinfo.m.offset);

  if (buffer_start == MAP_FAILED){
    ROS_ERROR("flir_boson_usb - mmap error. Failed to create a memory map for buffer.");
    return false;
  }

  // Fill this buffer with ceros. Initialization. Optional but nice to do
  memset(buffer_start, 0, bufferinfo.length);

  // Activate streaming
  int type = bufferinfo.type;
  if (ioctl(fd, VIDIOC_STREAMON, &type) < 0){
    ROS_ERROR("flir_boson_usb - VIDIOC_STREAMON error. Failed to activate streaming on the camera.");
    return false;
  }

  // Declarations for RAW16 representation
  // Will be used in case we are reading RAW16 format
  // Boson320 , Boson 640
  // OpenCV input buffer  : Asking for all info: two bytes per pixel (RAW16)  RAW16 mode`
  thermal16 = Mat(height, width, CV_16U, buffer_start);
  // OpenCV output buffer : Data used to display the video
  thermal16_linear = Mat(height, width, CV_8U, 1);

  // Declarations for 8bits YCbCr mode
  // Will be used in case we are reading YUV format
  // Boson320, 640 :  4:2:0
  int luma_height = height+height/2;
  int luma_width = width;
  int color_space = CV_8UC1;

  // Declarations for Zoom representation
  // Will be used or not depending on program arguments
  thermal_luma = Mat(luma_height, luma_width,  color_space, buffer_start);  // OpenCV input buffer
  // OpenCV output buffer , BGR -> Three color spaces :
  // (640 - 640 - 640 : p11 p21 p31 .... / p12 p22 p32 ..../ p13 p23 p33 ...)
  thermal_rgb = Mat(height, width, CV_8UC3, 1);

  return true;
}

bool BosonCamera::closeCamera(){
  // Finish loop. Exiting.
  // Deactivate streaming
  int type = bufferinfo.type;
  if (ioctl(fd, VIDIOC_STREAMOFF, &type) < 0 )
  {
    ROS_ERROR("flir_boson_usb - VIDIOC_STREAMOFF error. Failed to disable streaming on the camera.");
    return false;
  };

  close(fd);

  return true;
}


//Gather and store 16 bit images from the camera, publish them as 16b
void BosonCamera::captureAndPublish16b(ros::Time time_now)
{ 

  Size size(640, 512);

  sensor_msgs::CameraInfoPtr ci(new sensor_msgs::CameraInfo(camera_info->getCameraInfo()));
  cameraInfo = ci;

  cameraInfo->header.frame_id = frame_id;

  // Put the buffer in the incoming queue.
  if (ioctl(fd, VIDIOC_QBUF, &bufferinfo) < 0)
  {
    ROS_ERROR("flir_boson_usb - VIDIOC_QBUF error. Failed to queue the image buffer.");
    return;
  }

  // The buffer's waiting in the outgoing queue.
  if (ioctl(fd, VIDIOC_DQBUF, &bufferinfo) < 0)
  {
    ROS_ERROR("flir_boson_usb - VIDIOC_DQBUF error. Failed to dequeue the image buffer.");
    return;
  }


  if (video_mode == RAW16)
  {
     // RAW16 DATA
    cv_img_16b.image = thermal16;
    cv_img_16b.header.stamp = time_now;
    cv_img_16b.header.frame_id = frame_id;
    cv_img_16b.encoding = sensor_msgs::image_encodings::MONO16;
    pub_image_16b = cv_img_16b.toImageMsg();

    cameraInfo->header.stamp = pub_image_16b->header.stamp;
    image_pub_16b.publish(pub_image_16b, cameraInfo);
  }

}

// AGC Sample ONE: Linear from min to max.
// Input is a MATRIX (height x width) of 16bits. (OpenCV mat)
// Output is a MATRIX (height x width) of 8 bits (OpenCV mat)
void BosonCamera::agcBasicLinear(const Mat& input_16,
                                 Mat* output_8,
                                 const int& height,
                                 const int& width)
{
  int i, j;  // aux variables

  // auxiliary variables for AGC calcultion
  unsigned int max1 = 0;         // 16 bits
  unsigned int min1 = 0xFFFF;    // 16 bits
  unsigned int value1, value2, value3, value4;

  // RUN a super basic AGC
  for (i = 0; i < height; i++)
  {
    for (j = 0; j < width; j++)
    {
      value1 = input_16.at<uchar>(i, j * 2 + 1) & 0xFF;  // High Byte
      value2 = input_16.at<uchar>(i, j * 2) & 0xFF;      // Low Byte
      value3 = (value1 << 8) + value2;

      if (value3 <= min1)
        min1 = value3;

      if (value3 >= max1)
        max1 = value3;
    }
  }

  for (int i = 0; i < height; i++)
  {
    for (int j = 0; j < width; j++)
    {
      value1 = input_16.at<uchar>(i, j * 2 + 1) & 0xFF;     // High Byte
      value2 = input_16.at<uchar>(i, j * 2) & 0xFF;         // Low Byte
      value3 = (value1 << 8) + value2;
      value4 = ((255 * (value3 - min1))) / (max1 - min1);

      output_8->at<uchar>(i, j) = static_cast<uint8_t>(value4 & 0xFF);
    }
  }
}



